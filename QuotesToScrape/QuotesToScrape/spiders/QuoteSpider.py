'''
Modulo que implementa objeto, que extiende scrapy.Spider, y se encarga de recorrer las paginas con citas del sitio 
http://quotes.toscrape.com y extraer de estas paginas estaticas los textos de las citas, sus autores, y las 
etiquetas asociadas a estas.  
@version: 1.0
@since: 25-010-2018
@author: Humberto Gonzalez, Marcelo Rodriguez
'''
import scrapy
from QuotesToScrape.items import QuotesItem

import logging

"""
Clase con la funcionalidad que permite recorrer las paginas con citas del sitio http://quotes.toscrape.com 
y extraer los textos de las citas, sus autores, y las etiquetas asociadas a estas.
@extends: scrapy.Spider     
"""    
class QuoteSpider(scrapy.Spider):
    name = "Quotes to Scrape Spider"
    start_urls = ["http://quotes.toscrape.com"]
        
    """ Funcion para extraer de una pagina web los textos de las citas, sus autores, y las etiquetas asociadas a estas.
    @param response: Objeto que almacena la informacion de repuesta a una soliciud de una pagina web estatica 
    """             
    def parse(self, response):
        logging.info("QuoteSpider - parse - URL: %s" % response.url)
        for quote in response.css('div.quote'):
            yield QuotesItem(
                autor = quote.css('small.author::text').extract_first(),
                cita = quote.css('span.text::text').extract_first(),                
                etiquetas = quote.css('div.tags a.tag::text').extract(),
            )

        next_page =  response.css('li.next a::attr(href)').extract_first()
        if(next_page is not None):
            yield response.follow(next_page, self.parse)        
            
            