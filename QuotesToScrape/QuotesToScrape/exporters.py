'''
Modulo que implementa el objeto, que extiende de la clase BaseItemExporter, y que se encarga de exportar la informacion 
del texto de una cita, su autor, y las etiquetas asociadas a esta, en un archivo con sentencias SQL que permite 
cargar toda la informacion en una base de datos 
datos   
@version: 1.0
@since: 25-010-2018
@author: Humberto Gonzalez, Marcelo Rodriguez
@see: Modulo QuotesToScrape.items
'''
import logging
from scrapy.exporters import BaseItemExporter

"""
Clase con funcionalidad para crear las sentencias SQL con informacion de las citas y agregarlas a un archivo 
de exportacion que permite cargar toda la informacion en una base de datos
@extends: BaseItemExporter 
"""    
class SqlStmtItemExporter(BaseItemExporter):

    def __init__(self, file, **kwargs):
        self._configure(kwargs)
        self.file = file
        self.autores = []
        self.citas = []
        self.etiquetas = []

    """ Funcion que agrega un string al final de una lista en caso que no exista. Luego, retorna la posicion del 
    string dentro de la lista
    @param l: Objeto de tipo lista
    @param v: Objeto de tipo string
    @return: posicion del string v dentro de la lista l   
    """     
    def addItemGetIndex(self, l, v):
        if v not in l:
            l.append(v)
        return l.index(v)            

    """ Funcion para crear las sentencias SQL con informacion de las citas y copiarlas al archivo de exportacion
    @param item: Objeto QuotesItem con los datos de una cita (autor, texto, y etiquetas)
    @return: objeto item 
    @see: Modulo QuotesToScrape.items 
    """     
    def export_item(self, item):  
        try:       
            autor = item['autor']#.encode('utf-8')
            cita = item['cita']#.encode('utf-8')
            
            if autor not in self.autores:
                self.file.write("INSERT INTO autor(nombre) VALUES(\"%s\");\n" % autor.replace('"', '""'))
            idAutor = self.addItemGetIndex(self.autores, autor)                                     
            self.file.write("INSERT INTO cita(texto, autor_id) VALUES(\"%s\", %d);\n" % (cita.replace('"', '""'), idAutor))
            
            self.citas.append(cita)
            idCita = len(self.citas) - 1;        
                    
            for etq in item['etiquetas']:
                #etq = etq.encode('utf-8')
                if etq not in self.etiquetas:
                    self.file.write("INSERT INTO etiqueta(nombre) VALUES(\"%s\");\n" % etq.replace('"', '""'))
                idEtiqueta = self.addItemGetIndex(self.etiquetas, etq)
                self.file.write("INSERT INTO cita_etiqueta(cita_id, etiqueta_id) VALUES(%d, %d);\n" % (idCita, idEtiqueta))
        except Exception as err:
            logging.error("SqlStmtItemExporter, export_item(): %s" % err)        
        return item            
        
    def start_exporting(self):
        self.file.write("DELETE FROM cita_etiqueta;\n")
        self.file.write("DELETE FROM etiqueta;\n")
        self.file.write("DELETE FROM cita;\n")
        self.file.write("DELETE FROM autor;\n")

    def finish_exporting(self):
        pass
