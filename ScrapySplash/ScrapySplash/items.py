'''
Modulo que implementa el objeto, que extiende de la clase de scrapy.Item, y que se encarga de almacenar la información
de agencias, destinos, y hoteles.
@version: 1.0
@since: 05-11-2018
@author: Humberto Gonzalez, Marcelo Rodriguez
@see: Modulo ScrapySplash.spiders.SplashSpider
'''
import scrapy

  
class ScrapysplashItem(scrapy.Item):
    pass

"""
Clase que contiene los campos para almacenar el nombre de una agencia, el nombre de un destino, el nombre de un
hotel, y la url del hotel en el sitio web de la agencia 
@extends: scrapy.Item     
"""
class HotelItem(scrapy.Item):
    agencia = scrapy.Field()
    destino = scrapy.Field()
    hotel = scrapy.Field()
    url = scrapy.Field()