import scrapy
import requests 
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.exceptions import CloseSpider
from TallerB.items import TallerbItem
import logging
from urllib.parse import urlparse

class InfSpider(CrawlSpider):
    handle_httpstatus_list = [301, 302, 404]
    name = "InfSpider"
    web_count = 0
    allowed_domains = ['inf.ucv.cl' ]
    start_urls = ['http://www.inf.ucv.cl/' ]
    historial = ['http://www.inf.ucv.cl/']
    next_url = []

    rules = [
            Rule(
                LinkExtractor(
                    canonicalize=True,
                    unique=True
                ),
                follow=True,
                callback="parse"
            )
        ]

    def start_requests(self):
        for url in self.start_urls:
            yield scrapy.Request(url, callback=self.parse, dont_filter=True)

    def parse(self, response):
        hxs = scrapy.Selector(response)
        # extract all links from page
        all_links = hxs.xpath('*//a/@href').extract()
        # iterate over links
        respon = requests.get(response.url)
        if respon.status_code == 301:
            logging.info('////////////////////////////////////')
            logging.info(respon.url)
            logging.info(respon.status_code)
            logging.info(respon.headers['content-type'])
            logging.info(respon.headers['content-length'])
        for link in all_links:
            if urlparse(link).scheme:
                yield scrapy.http.Request(url=link, callback= self.parse_items)

    def parse_items(self, response):
        links = LinkExtractor(canonicalize=True, unique=True).extract_links(response)
        outpu=[]
        temp = requests.get(response.url)
        logging.info(temp.headers)
        if not 'content-length' in temp.headers:
            temp.headers['content-length'] = 0 

        for link in links:
            is_allowed = False
            for allowed_domain in self.allowed_domains:
                if allowed_domain in link.url:
                    is_allowed = True
            if is_allowed:
                if not link.url in self.historial:
                    self.next_url.append(link.url)
                    self.historial.append(link.url)
                    outpu.append(link.url)
        yield TallerbItem(
                    url = response.url,
                    estado = temp.status_code,
                    tipo = temp.headers['content-type'],
                    largo = temp.headers['content-length'],
                    salida = outpu
                )
        # Return all the found items
        if len(self.next_url) is not 0:
            yield response.follow(self.next_url.pop(0), self.parse_items)
            