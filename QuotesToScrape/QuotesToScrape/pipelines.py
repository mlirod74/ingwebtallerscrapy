'''
Modulo que implementa los objetos Pipeline encargados de procesar la informacion extraida por el objeto QuoteSpider 
@version: 1.0
@since: 25-010-2018
@author: Humberto Gonzalez, Marcelo Rodriguez
@see: Modulo QuotesToScrape.spiders.QuoteSpider
@see: Modulo QuotesToScrape.items
'''
import logging
import sqlite3
from QuotesToScrape.exporters import SqlStmtItemExporter

""" Funcionalidad ejecutada al cargar el modulo y que tiene como objetivo establecer una conexion a la base de datos
y el borrado de las tablas que contienen la informacion de las citas  
"""               
try:
    conn = sqlite3.connect('../database/QuotesToScrape.db')
    cursor = conn.cursor()
    cursor.execute("DELETE FROM cita_etiqueta;")
    cursor.execute("DELETE FROM etiqueta;")
    cursor.execute("DELETE FROM cita;")
    cursor.execute("DELETE FROM autor;")        
    conn.commit()
except Exception as err:
    logging.error("SQL error: %s" % err)  
    


class QuotestoscrapePipeline(object):
    def process_item(self, item, spider):
        return item

"""
Clase Pipeline con la funcionalidad para limitar el texto de una cita a 255 caracteres 
"""    
class MaxLengthPipeline(object):
    """ Funcion para recortar el texto de una cita a 255 caracteres
    @param item: Objeto QuotesItem con los datos de una cita (autor, texto, y etiquetas)
    @param spider: Objeto spider que extrajo la infomacion de las citas
    @return: objeto item 
    @see: Modulo QuotesToScrape.items 
    """               
    def process_item(self, item, spider):
        if(item['cita'] is not None):
            item['cita'] = (item['cita'])[:255]
        return item    

"""
Clase Pipeline con la funcionalidad insertar la informacion de las citas en una base de datos  
"""        
class SQLiteCitasPipeline(object):
    """ Funcion que inserta en la base de tatos los datos de una cita  
    @param item: Objeto QuotesItem con los datos de una cita (autor, texto, y etiquetas)
    @param spider: Objeto spider que extrajo la infomacion de las citas
    @return: objeto item 
    @see: Modulo QuotesToScrape.items 
    """    
    def process_item(self, item, spider):
        try:  
            sql = "INSERT OR IGNORE INTO autor(nombre) VALUES(?);" 
            val = (item['autor'],)        
            cursor.execute(sql ,val)
            sql = "INSERT INTO cita(texto, autor_id) VALUES(?, (select id from autor where nombre = ?));"
            val = (item['cita'], item['autor'])
            cursor.execute(sql ,val)
            cita_id = cursor.lastrowid
            for tag in item['etiquetas']:
                sql = "INSERT OR IGNORE INTO etiqueta(nombre) VALUES(?);"
                val = (tag,)
                cursor.execute(sql, val)  
                sql = "INSERT INTO cita_etiqueta(cita_id, etiqueta_id) VALUES(?, (select id from etiqueta where nombre = ?));"
                val = (cita_id,tag)
                cursor.execute(sql, val)  
            conn.commit()
        except Exception as err:
            logging.error("SQLiteCitasPipeline, process_item(): %s" % err)        
        return item

"""
Clase Pipeline con la funcionalidad para crear un archivo de exportacion con setencias sql para cargar en una base
de datos la informacion de las citas 
"""            
class SqlStmtPipeline(object):
    """ Funcion que abre el archivo de exportacion para las citas  
    @param spider: Objeto spider que extrajo la infomacion de las citas 
    """        
    def open_spider(self, spider):
        try:
            self.file = open('../database/InsertItems.sql', 'w')
            self.exporter = SqlStmtItemExporter(self.file)
            self.exporter.start_exporting()
        except Exception as err:
            logging.error("SqlStmtPipeline, open_spider(): %s" % err)
                
    """ Funcion que cierra el archivo de exportacion para las citas  
    @param spider: Objeto spider que extrajo la infomacion de las citas 
    """        
    def close_spider(self, spider):
        try:  
            self.exporter.finish_exporting()
            self.file.close()
        except Exception as err:
            logging.error("SqlStmtPipeline, close_spider(): %s" % err)        

    """ Funcion para agregar los datos de una cita al archivo de exportacion
    @param item: Objeto QuotesItem con los datos de una cita (autor, texto, y etiquetas)
    @param spider: Objeto spider que extrajo la infomacion de las citas
    @return: objeto item 
    @see: Modulo QuotesToScrape.items 
    """            
    def process_item(self, item, spider):
        try:  
            self.exporter.export_item(item)
        except Exception as err:
            logging.error("SqlStmtPipeline, process_item(): %s" % err)        
        return item    

