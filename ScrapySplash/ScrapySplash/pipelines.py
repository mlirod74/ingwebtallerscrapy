'''
Modulo que implementa los objetos Pipeline encargados de procesar la informacion extraida por el objeto SplashSpider 
@version: 1.0
@since: 05-11-2018
@author: Humberto Gonzalez, Marcelo Rodriguez
@see: Modulo ScrapySplash.spiders.SplashSpider
@see: Modulo ScrapySplash.items
'''
import logging

import sqlite3
from urllib.parse import urlparse
from scrapy.exporters import CsvItemExporter

class ScrapysplashPipeline(object):
    pass

""" Funcionalidad ejecutada al cargar el modulo y que tiene como objetivo establecer una conexion a la base de datos
y el borrado de las tablas que contienen la informacion de los hoteles y los destinos  
"""
try:
    conn = sqlite3.connect('../database/ScrapySplash.db')
    cursor = conn.cursor()
    cursor.execute("DELETE FROM hotel;")
    cursor.execute("DELETE FROM destino;")
    conn.commit()
except Exception as err:
    logging.error("SQL error: %s" % err)


"""
Clase Pipeline con la funcionalidad para limitar los texto de los nombres y de las URLs 
"""
class MaxLengthPipeline(object):
    """ Funcion para recortar los textos de nombres y de las URLs a la capacidad del campo en la base de datos
    @param item: Objeto HotelItem con los datos de una agencia, un destino, y un hotel
    @param spider: Objeto spider que extrajo la infomacion de las paginas web de las agencias
    @return: objeto item 
    @see: Modulo ScrapySplash.items 
    """
    def process_item(self, item, spider):
        if(item['hotel'] is not None):
            item['hotel'] = (item['hotel'])[:40]
        if(item['url'] is not None):
            item['url'] = (item['url'])[:255]
        if(item['destino'] is not None):
            item['destino'] = (item['destino'])[:30]                        
        return item



"""
Clase Pipeline con la funcionalidad insertar la informacion de hoteles y destinos en una base de datos  
"""
class SqlitePipeline(object):
    agencia_destinos = set()
    """ Funcion que inserta en la base de datos los datos informacion de hoteles y destinos
    @param item: Objeto HotelItem con los datos de una agencia, un destino, y un hotel
    @param spider: Objeto spider que extrajo la infomacion de las paginas web de las agencias
    @return: objeto item 
    @see: Modulo ScrapySplash.items 
    """
    def process_item(self, item, spider):
        try:
            nombre_agencia = item['agencia']
            nombre_hotel = item['hotel']
            nombe_destino = item['destino']
            url_hotel = item['url']
            if (nombre_agencia+"_"+nombe_destino) not in self.agencia_destinos:
                self.agencia_destinos.add(nombre_agencia+"_"+nombe_destino)
                sql = "INSERT INTO destino(nombre, agencia_id) VALUES(?, (select id from agencia where nombre = ?));"
                val = (nombe_destino, nombre_agencia)
                cursor.execute(sql, val)            
            sql = "INSERT INTO hotel(nombre, url, destino_id) VALUES(?, ?, (select id from destino where nombre = ? and agencia_id = (select id from agencia where nombre = ?)));"
            val = (nombre_hotel, url_hotel, nombe_destino, nombre_agencia)
            cursor.execute(sql, val)
            conn.commit()            
        except Exception as err:
             logging.error("SqliteScrapySplashPipeline, process_item(): %s" % err)
        return item
            
"""
Clase Pipeline con la funcionalidad para crear un archivo tipo cvs con la informacion de las agencias, los destinos,
los hoteles y las URLs de estos en los sitios web de las agencias  
"""            
class ExportCvsPipeline(object):
    """ Funcion que abre el archivo cvs para almacenar la informacion  
    @param spider: Objeto spider que extrajo la infomacion de las paginas web de las agencias
    """        
    def open_spider(self, spider):
        try:
            self.file = open('../ListadoHoteles.cvs', 'wb')
            self.exporter = CsvItemExporter(self.file)
            self.exporter.start_exporting()
        except Exception as err:
            logging.error("ExportCvsPipeline, open_spider(): %s" % err)
                
    """ Funcion que cierra el archivo cvs 
    @param spider: Objeto spider que extrajo la infomacion de las paginas web de las agencias 
    """        
    def close_spider(self, spider):
        try:  
            self.exporter.finish_exporting()
            self.file.close()
        except Exception as err:
            logging.error("ExportCvsPipeline, close_spider(): %s" % err)        

    """ Funcion para agregar los datos de de las agencias, los destinos, los hoteles y sus URLs
    @param item: Objeto HotelItem con los datos de una agencia, un destino, y un hotel
    @param spider: Objeto spider que extrajo la infomacion de las paginas web de las agencias
    @return: objeto item 
    @see: Modulo ScrapySplash.items  
    """            
    def process_item(self, item, spider):
        try:  
            self.exporter.export_item(item)
        except Exception as err:
            logging.error("ExportCvsPipeline, process_item(): %s" % err)        
        return item    
        