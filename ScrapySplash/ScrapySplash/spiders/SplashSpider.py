'''
Modulo que implementa objeto, que extiende scrapy.Spider, y se encarga de extraer algunas paginas 
de los sitios web de Tripadvisor y Booking
@version: 1.0
@since: 05-11-2018
@author: Humberto Gonzalez, Marcelo Rodriguez
'''
import scrapy
from scrapy.selector import Selector
from scrapy_splash import SplashRequest
from ScrapySplash.items import HotelItem

import logging
from urllib.parse import urlparse

"""
Clase con la funcionalidad para extraer la infomación de hoteles, destinos, y agencias, de algunas paginas de los
sitios web de  de Tripadvisor y Booking
@extends: scrapy.Spider     
"""    
class SplashSpider(scrapy.Spider):
    name = "Splash Scrape Spider"    
    # Paginas con listados de hoteles de Cancun, Varadero, y valparaiso, de Tripadvisor y Booking 
    start_urls = ["https://www.tripadvisor.cl/Hotels-g150807-Cancun_Yucatan_Peninsula-Hotels.html",
                  "https://www.tripadvisor.cl/Hotels-g147275-Varadero_Matanzas_Province_Cuba-Hotels.html", 
                  "https://www.tripadvisor.cl/Hotels-g294306-Valparaiso_Valparaiso_Region-Hotels.html",
                  "https://www.booking.com/searchresults.es.html?label=gen173nr-1DCAQoggJCDGNpdHlfLTU5NzExOEgKWARoL4gBAZgBCrgBGcgBD9gBA-gBAfgBAogCAagCAw&lang=es&sid=c633618b7dadd7dd44472b5046949cd1&sb=1&src=searchresults&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Fsearchresults.es.html%3Flabel%3Dgen173nr-1DCAQoggJCDGNpdHlfLTU5NzExOEgKWARoL4gBAZgBCrgBGcgBD9gBA-gBAfgBAogCAagCAw%3Bsid%3Dc633618b7dadd7dd44472b5046949cd1%3Bcheckin_month%3D12%3Bcheckin_monthday%3D13%3Bcheckin_year%3D2018%3Bcheckout_month%3D12%3Bcheckout_monthday%3D29%3Bcheckout_year%3D2018%3Bcity%3D-1647644%3Bclass_interval%3D1%3Bdest_id%3D-1647644%3Bdest_type%3Dcity%3Bdtdisc%3D0%3Bfrom_sf%3D1%3Bgroup_adults%3D2%3Bgroup_children%3D0%3Binac%3D0%3Bindex_postcard%3D0%3Blabel_click%3Dundef%3Bno_rooms%3D1%3Boffset%3D0%3Bpostcard%3D0%3Broom1%3DA%252CA%3Bsb_price_type%3Dtotal%3Bshw_aparth%3D1%3Bslp_r_match%3D0%3Bsrc%3Dsearchresults%3Bsrc_elem%3Dsb%3Bsrpvid%3Df4960a9d087400d6%3Bss%3DVaradero%3Bss_all%3D0%3Bssb%3Dempty%3Bsshis%3D0%3Bssne%3DVaradero%3Bssne_untouched%3DVaradero%26%3B&ss=Varadero&ssne=Varadero&ssne_untouched=Varadero&city=-1647644&checkin_monthday=25&checkin_month=2&checkin_year=2019&checkout_monthday=27&checkout_month=2&checkout_year=2019&no_rooms=1&group_adults=2&group_children=0&b_h4u_keep_filters=&from_sf=1",
                  "https://www.booking.com/searchresults.es.html?label=gen173nr-1DCAQoggJCDGNpdHlfLTU5NzExOEgKWARoL4gBAZgBCrgBGcgBD9gBA-gBAfgBAogCAagCAw&lang=es&sid=c633618b7dadd7dd44472b5046949cd1&sb=1&src=searchresults&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Fsearchresults.es.html%3Flabel%3Dgen173nr-1DCAQoggJCDGNpdHlfLTU5NzExOEgKWARoL4gBAZgBCrgBGcgBD9gBA-gBAfgBAogCAagCAw%3Bsid%3Dc633618b7dadd7dd44472b5046949cd1%3Bcheckin_month%3D2%3Bcheckin_monthday%3D25%3Bcheckin_year%3D2019%3Bcheckout_month%3D2%3Bcheckout_monthday%3D27%3Bcheckout_year%3D2019%3Bcity%3D-1647644%3Bclass_interval%3D1%3Bdest_id%3D-1647644%3Bdest_type%3Dcity%3Bdtdisc%3D0%3Bfrom_sf%3D1%3Bgroup_adults%3D2%3Bgroup_children%3D0%3Binac%3D0%3Bindex_postcard%3D0%3Blabel_click%3Dundef%3Bno_rooms%3D1%3Boffset%3D0%3Bpostcard%3D0%3Broom1%3DA%252CA%3Bsb_price_type%3Dtotal%3Bshw_aparth%3D1%3Bslp_r_match%3D0%3Bsrc%3Dsearchresults%3Bsrc_elem%3Dsb%3Bsrpvid%3D23160ab837480100%3Bss%3DVaradero%3Bss_all%3D0%3Bssb%3Dempty%3Bsshis%3D0%3Bssne%3DVaradero%3Bssne_untouched%3DVaradero%26%3B&ss=Canc%C3%BAn%2C+Quintana+Roo%2C+M%C3%A9xico&ssne=Varadero&ssne_untouched=Varadero&city=-1647644&checkin_monthday=25&checkin_month=2&checkin_year=2019&checkout_monthday=27&checkout_month=2&checkout_year=2019&no_rooms=1&group_adults=2&group_children=0&b_h4u_keep_filters=&from_sf=1&ss_raw=canc&ac_position=0&ac_langcode=es&ac_click_type=b&dest_id=-1655011&dest_type=city&iata=CUN&place_id_lat=21.161314&place_id_lon=-86.834129&search_pageview_id=23160ab837480100&search_selected=true&search_pageview_id=23160ab837480100&ac_suggestion_list_length=5&ac_suggestion_theme_list_length=0",
                  "https://www.booking.com/searchresults.es.html?label=gen173nr-1DCAEoggI46AdIM1gEaC-IAQGYAQq4ARnIAQ_YAQPoAQGIAgGoAgM&lang=es&sid=c633618b7dadd7dd44472b5046949cd1&sb=1&src=searchresults&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Fsearchresults.es.html%3Flabel%3Dgen173nr-1DCAEoggI46AdIM1gEaC-IAQGYAQq4ARnIAQ_YAQPoAQGIAgGoAgM%3Bsid%3Dc633618b7dadd7dd44472b5046949cd1%3Bcheckin_month%3D2%3Bcheckin_monthday%3D6%3Bcheckin_year%3D2019%3Bcheckout_month%3D2%3Bcheckout_monthday%3D8%3Bcheckout_year%3D2019%3Bclass_interval%3D1%3Bdest_id%3D12%3Bdest_type%3Dcountry%3Bdtdisc%3D0%3Bfrom_sf%3D1%3Bgroup_adults%3D2%3Bgroup_children%3D0%3Binac%3D0%3Bindex_postcard%3D0%3Blabel_click%3Dundef%3Bno_rooms%3D1%3Boffset%3D0%3Bpostcard%3D0%3Braw_dest_type%3Dcountry%3Broom1%3DA%252CA%3Bsb_price_type%3Dtotal%3Bshw_aparth%3D1%3Bslp_r_match%3D0%3Bsrc%3Dsearchresults%3Bsrc_elem%3Dsb%3Bsrpvid%3D49066d367b2302a7%3Bss%3DAruba%3Bss_all%3D0%3Bssb%3Dempty%3Bsshis%3D0%3Bssne%3DAruba%3Bssne_untouched%3DAruba%26%3B&ss=Valpara%C3%ADso%2C+Valpara%C3%ADso+Region%2C+Chile&ssne=Aruba&ssne_untouched=Aruba&checkin_monthday=6&checkin_month=2&checkin_year=2019&checkout_monthday=8&checkout_month=2&checkout_year=2019&no_rooms=1&group_adults=2&group_children=0&b_h4u_keep_filters=&from_sf=1&ss_raw=valpara&ac_position=0&ac_langcode=es&ac_click_type=b&dest_id=-904020&dest_type=city&place_id_lat=-33.04261&place_id_lon=-71.623459&search_pageview_id=49066d367b2302a7&search_selected=true&search_pageview_id=49066d367b2302a7&ac_suggestion_list_length=5&ac_suggestion_theme_list_length=0"]

    """ Funcion que usa la libreria scrapy splash para solicitar al servicio scrapinghub/splash que recupere 
    el contenido dinamico desde una url y lo devuelva como html    
    """       
    def start_requests(self):
        for url in self.start_urls:
            yield SplashRequest(url, self.parse,
                endpoint='render.html',
                args={'wait': 1.0},
            )
            
    """ Funcion para extraer información de hoteles de una pagina web
    @param response: Objeto que almacena la informacion de repuesta a una solicitud de una pagina web dinamica 
    """       
    def parse(self, response):
        logging.info("SplashSpider - parse - URL: %s" % response.url)
        baseUrl = urlparse(response.url).scheme+"://"+urlparse(response.url).netloc
        agc = urlparse(response.url).netloc.split('.')        
        for div in response.xpath("//div[@class='listing_title']").extract():
            yield HotelItem(
                agencia = agc[1],
                destino = response.xpath("//span[@class='ui_pill inverted']/text()").extract()[0],
                hotel = Selector(text=div).xpath('//a/text()').extract()[0],
                url = baseUrl+Selector(text=div).xpath('//a/@href').extract()[0]
                )             
        for anchor in response.xpath("//a[starts-with(@class,'hotel_name_link')]").extract():            
            yield HotelItem(
                agencia = agc[1],
                destino = response.xpath("//input[@id='ss']/@value").extract()[0],
                hotel = Selector(text=anchor).xpath("//span[starts-with(@class,'sr-hotel__name')]/text()").extract()[0].replace('\n', ' '),
                url = baseUrl+Selector(text=anchor).xpath('//@href').extract()[0]
                )                                     
        
