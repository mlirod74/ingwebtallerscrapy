'''
Modulo que implementa el objeto, que extiende de la clase de scrapy.Item, y que se encarga de almacenar la informacion 
del texto de una cita, su autor, y las etiquetas asociadas a esta, obtenidos de la busqueda y extraccion que efectua 
el objeto QuoteSpider
@version: 1.0
@since: 25-010-2018
@author: Humberto Gonzalez, Marcelo Rodriguez
@see: Modulo QuotesToScrape.spiders.QuoteSpider
'''
import scrapy


class QuotestoscrapeItem(scrapy.Item):
    # define the fields for your item here like:
    pass

"""
Clase que contiene los campos (propiedades) para almacenar el texto, autor, y las etiquetas de una cita 
@extends: scrapy.Item     
"""    
class QuotesItem(scrapy.Item):
    autor = scrapy.Field()
    cita = scrapy.Field()
    etiquetas = scrapy.Field()
    